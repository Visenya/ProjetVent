#!/usr/bin/gawk -f

# Programme permettant de créer le fichier contenant le code shell pour ouvrir une bo$ite de dialogue.
# Nécessaire pour l'option interactive de la commande restore
  BEGIN {max=ARGV[2]; ARGV[2]="";}

  NR == 1 { print ("#!/usr/bin/env bash");
    print ("DIALOG=${DIALOG=dialog}");
    print ("fichtemp=`tempfile 2>/dev/null` || fichtemp=/temp/test$$");
    print ("trap \"rm -f $fichtemp\" 0 1 2 5 15");
    print ("$DIALOG --stdout --title \"Checklist dialog\" --clear \\");
    print ("--checklist \"What file do you want to restore ?\"  24 60 10 \\");
    print ("\""$6"\"","\""$1"\"","off \\");
  }



  NR != 1 && NR != max {print ("\""$6"\"","\""$1"\"","off \\");}
  NR == max {print ("\""$6"\"","\""$1"\"","off 2>$fichtemp");}
  END {
  print ("choix=`cat $fichtemp`");
  print ("case $? in");
  print ("0) echo \"$choix\" ;;");
  print ("1)  echo \"Type on cancel button\";;");
  print ("255)  echo \"Type on escape button\";;");
  print ("esac");
  }
