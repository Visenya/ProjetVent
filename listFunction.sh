#!/usr/bin/env bash

#Set fonts for Help.
NORM=`tput sgr0`
BOLD=`tput bold`

# Permet d'effacer le contenu du fichier temporaire
function effacerFichier {
  if [ -s ~/bin/Temp/FichTemp.txt ]; then
     > ~/bin/Temp/FichTemp.txt
  fi
}

# Permet de verifier si le dossier et les fichiers temporaires existe et création de la base de données temporaires
function VerifTemp {
  if [ ! -d ~/bin/Temp/ ]; then
    mkdir ~/bin/Temp/
    touch ~/bin/Temp/FichTemp.txt
  else
    # echo $name
    echo "$dateClassic $dateFichier $taille $nickname $road $name" >> ~/bin/Temp/FichTemp.txt
  fi
}

# Renvoie un message d'aide oncernant la liste des commandes possibles pour la commande restore
function aukesur {
  echo
	echo "${BOLD}  Descritpion${NORM}"
	echo -e "  restore restore each specified files or folders, which are in Trash."\\n
	echo -e "${BOLD}$SCRIPT  Basic usage:${NORM}  file.ext${NORM}"\\n
	echo -e "  Command line switches are optional. The following switches are recognized."\\n
	echo "    -d  --Restore files by date. A date is necessary for this command. Format is : 2017/03/24-2016/03/24."
  echo "    -p  --Restore files according to their path. A path is necessary for this command."
  echo "    -o  --Restore the oldest file."
  echo "    -n  --Restore the newest file."
  echo "    -s  --Restore the smallest file."
	echo "    -l  --Restore the largest file."
  echo "    -i  --Open a dialog window authorising selection of files to restore."
	echo -e "    -h  --Displays this help message. No further functions are performed."\\n
	echo -e "${BOLD}$SCRIPT  Example:${NORM} restore -d  date -l "\\n
	exit 1
}

# Renvoie un message d'aide oncernant la liste des commandes possibles pour la commande del
function helpme {
  echo
  echo " ${BOLD} Description${NORM}"
  echo -e "  del removes each specified files or folders."\\n
  echo -e "  ${BOLD}Basic usage${NORM}:  file.ext"\\n
  echo -e "  Command line switch is optional. The following switch is recognized."\\n
  echo -e "    -h --help    --Displays this help message. No further functions are performed."\\n
  echo -e " ${BOLD} Example${NORM}:  del file.ext"\\n
}

# Permet la restauratiion de fichiers selon un intervalle de date
function thedate {
  local date1=$(echo $rDATE | cut -d"-" -f1 | tr '/' '-')
  date1=$(echo `date -d "$date1" +%s`)
  local date2=$(echo $rDATE | cut -d"-" -f2 | tr '/' '-')
  date2=$(echo `date -d "$date2" +%s`)

  effacerFichier

  cat < ~/bin/sauvegarde.txt | while true
  do
    read ligne
    if [ "$ligne" = "" ]; then
      break;
    fi

    local dateClassic=$(echo ${ligne} | cut -d" " -f1)
    local dateFichier=$(echo ${ligne} | cut -d" " -f2)
    local taille=$(echo ${ligne} | cut -d" " -f3)
    local nickname=$(echo ${ligne} | cut -d" " -f4)
    local road=$(echo ${ligne} | cut -d" " -f5)
    local name=$(echo ${ligne} | cut -d" " -f6)

    if [ $dateFichier -lt $date2 ] && [ $dateFichier -gt $date1 ]; then
      if [ $rSMALLEST -eq 1 ]; then
        VerifTemp
      else
        if [ $rLARGEST -eq 1 ]; then
          VerifTemp
        else
          if [ $rNEWEST -eq 1 ]; then
            VerifTemp
          else
            if [ $rOLDEST -eq 1 ]; then
              VerifTemp
            else
              mv "${TEST}/bin/Trash/${nickname}" "${road}/${nickname}"
              mv "${road}/${nickname}" "${road}/${name}"
            fi
          fi
        fi
      fi
    fi
  done
}

#1487286000

# Permet la restauratiion de fichiers selon un chemin
function thepath {
  local result=$(grep -e "${TEST}/${rRESTORE_PATH}" ~/bin/sauvegarde.txt)

  effacerFichier

  echo "$result" | while true
  do
    read ligne
    if [ "$ligne" = "" ]; then
      break;
    fi

    local dateClassic=$(echo ${ligne} | cut -d" " -f1)
    local dateFichier=$(echo ${ligne} | cut -d" " -f2)
    local taille=$(echo ${ligne} | cut -d" " -f3)
    local nickname=$(echo ${ligne} | cut -d" " -f4)
    local road=$(echo ${ligne} | cut -d" " -f5)
    local name=$(echo ${ligne} | cut -d" " -f6)

      if [ $rSMALLEST -eq 1 ]; then
        VerifTemp
      else
        if [ $rLARGEST -eq 1 ]; then
          VerifTemp
        else
          if [ $rNEWEST -eq 1 ]; then
            VerifTemp
          else
            if [ $rOLDEST -eq 1 ]; then
              VerifTemp
            else
              mv "${TEST}/bin/Trash/${nickname}" "${road}/${nickname}"
              mv "${road}/${nickname}" "${road}/${name}"
            fi
          fi
        fi
    fi
  done
}

# Permet la restauratiion de fichiers le plus lourd
function thelarge {
  if [ $HOPE -eq 1 ] || [ $SATAN -eq 1 ] && [ $rLARGEST -eq 1 ]; then
    local poids=$(cut -d" " -f3 ~/bin/Temp/FichTemp.txt | sed '/^$/d' | sort -bg | tail -1)
    local result=$(grep -e " $poids " ~/bin/Temp/FichTemp.txt)

    moveTemp
    # echo "gg"
  else
    local poids=$(cut -d" " -f3 ~/bin/sauvegarde.txt | sed '/^$/d' | sort -bg | tail -1)
    local result=$(grep -e " $poids " ~/bin/sauvegarde.txt) #les guillemets permettent de préciser qu'on ne souhaite que les valeurs $poids entourées d'espaces des deux côtés.
    move
    # echo "lose"
  fi
}

# Permet la restauratiion de fichiers le plus petit
function thesmall {
  if [ $HOPE -eq 1 ] || [ $SATAN -eq 1 ] && [ $rSMALLEST -eq 1 ]; then
    local poids=$(cut -d" " -f3 ~/bin/Temp/FichTemp.txt | sed '/^$/d' | sort -bg | head -1)
    local result=$(grep -e " $poids " ~/bin/Temp/FichTemp.txt)
    # echo "gg"
    moveTemp
  else
    local poids=$(cut -d" " -f3 ~/bin/sauvegarde.txt | sed '/^$/d' | sort -bg | head -1)
    local result=$(grep -e " $poids " ~/bin/sauvegarde.txt)
    # echo "lose"
    move
  fi

}

# Permet la restauratiion de fichiers le plus récent
function thenew {
  if [ $HOPE -eq 1 ] || [ $SATAN -eq 1 ] && [ $rNEWEST -eq 1 ]; then
    local date=$(cut -d" " -f2 ~/bin/Temp/FichTemp.txt | sort | tail -1)
  	local result=$(grep -e " $date " ~/bin/Temp/FichTemp.txt | head -1)
    moveTemp
  else
    local date=$(cut -d" " -f2 ~/bin/sauvegarde.txt | sort | tail -1)
  	local result=$(grep -e " $date " ~/bin/sauvegarde.txt | head -1)
    move
  fi

}

# Permet la restauratiion de fichiers le plus ancien
function theold {
  if [ $HOPE -eq 1 ] || [ $SATAN -eq 1 ] && [ $rOLDEST -eq 1 ]; then
    local	date=$(cut -d" " -f2 ~/bin/Temp/FichTemp.txt | sort | head -1)
    local result=$(grep -e " $date " ~/bin/Temp/FichTemp.txt | head -1)
    moveTemp
  else
    local	date=$(cut -d" " -f2 ~/bin/sauvegarde.txt | sort | head -1)
    local result=$(grep -e " $date " ~/bin/sauvegarde.txt | head -1)
    move
  fi
}

# Permet de deplacer le fichier une fois l'action désirée effectuée
function move {
  local name=$(echo ${result} | cut -d" " -f6)
  local nickname=$(echo ${result} | cut -d" " -f4)
  local road=$(echo ${result} | cut -d" " -f5)

  mv "${TEST}/bin/Trash/${nickname}" "${road}/${nickname}"

  mv "${road}/${nickname}" "${road}/${name}"

  suppression2
}

# Permet de deplacer le fichier une fois l'action désirée effectuée pour réaliser plusieurs arguments
function moveTemp {
  local name=$(echo ${result} | cut -d" " -f6)
  local nickname=$(echo ${result} | cut -d" " -f4)
  local road=$(echo ${result} | cut -d" " -f5)

  mv "${TEST}/bin/Trash/${nickname}" "${road}/${nickname}"

  mv "${road}/${nickname}" "${road}/${name}"

  suppression2
}

# Permet de mettre à jour la base de données
function suppression2 {
  if [ -s ~/bin/Temp/CompTemp.txt ]; then
      > ~/bin/Temp/CompTemp.txt
   fi
    local res=$(ls ~/bin/Trash/ | sort -bg)
    grep -e " $res " ~/bin/sauvegarde.txt >> ~/bin/Temp/CompTemp.txt
    cat ~/bin/Temp/CompTemp.txt > ~/bin/sauvegarde.txt
}

# function gamewith {
#   local num=$(wc -l $HOME/bin/sauvegarde.txt | cut -d" " -f1)
#   ./~/bin/dialog.awk $HOME/bin/sauvegarde.txt $num > ~/bin/Temp/GLaDOS.sh
#   chmod +x ~/bin/Temp/GLaDOS.sh
#   donnees=$(.~/bin/Temp/GLaDOS.sh)
#   local result=$(grep -e " $donnes " ~/bin/sauvegarde.txt )
#
#   echo "$result" | while true
#   do
#     read ligne
#     if [ "$ligne" = "" ]; then
#       break;
#     fi
#
#     local nickname=$(echo ${ligne} | cut -d" " -f4)
#     local road=$(echo ${ligne} | cut -d" " -f5)
#     local name=$(echo ${ligne} | cut -d" " -f6)
#
#     mv "${TEST}/bin/Trash/${nickname}" "${road}/${nickname}"
#     mv "${road}/${nickname}" "${road}/${name}"
#   done
#
#   suppression2
# }
