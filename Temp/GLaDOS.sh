#!/usr/bin/env bash
DIALOG=${DIALOG=dialog}
fichtemp=`tempfile 2>/dev/null` || fichtemp=/temp/test$$
trap "rm -f $fichtemp" 0 1 2 5 15
$DIALOG --stdout --inputbox --title "Checklist dialog" --clear \
--checklist "What file do you want to restore ?"  24 60 10 \
"bouh.php" "2017-03-24" off \
"portal" "2017-03-24" off 2>$fichtemp
choix=`cat $fichtemp`
case $? in
0) echo "$choix" ;;
1)  echo "Type on cancel button";;
255)  echo "Type on escape button";;
esac
