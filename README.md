# Effacement / Restauration de fichiers et de répertoire



### Objectif

1. Supprimer un fichier ou un dossier et sauvegarder des informations relatives à ce dernier.
2. Restaurer un fichier ou dossier dans son environnement précédent la suppression.
3. Travailler en autonomie sur un projet.


### Installation

* Nécessite à environnement `Linux`.

* Vérifier la comptabilité avec les langages utilisés : `Bash`, `Awk`.

* Décompresser l'archive dans le `HOME` (`/home/usr`).

* Accorder les droits à l'ensemble du dossier `bin` => `chmod +x bin/`


### Running

1. Prenez le temps de lire le `Help` de chaque fonction `del` et `restore`
    => commande -h
    => commande --help
2. Lancer les commandes à partir de terminal peut importe le chemin
3. Admirez le travail


### Exemple

=> del Luuna

=> restore -s
